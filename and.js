function inputClick(clickedId) {
  console.log('clicked_id: ' + clickedId);

  _toggleClass(clickedId, 'active');

  if(_isClassExists('and-p', 'active') && _isClassExists('and-q', 'active')) {
    _addClass('and-out', 'active');
  } else {
    _removeClass('and-out', 'active');
  }

  _clearBackgroundsInTable();

  var idTable = '';
  idTable += _isClassExists('and-p', 'active') ? '1' : '0';
  idTable += _isClassExists('and-q', 'active') ? '1' : '0';
  _addClass(idTable, 'active');
}

function _clearBackgroundsInTable() {
  var el = document.getElementsByTagName('tr');
      
  for (var i = 0; i < el.length; i++) {
    el[i].classList.remove('active');
  }
}

function _toggleClass(id, className) {
  document.getElementById(id).classList.toggle(className);
}

function _addClass(id, className) {
  document.getElementById(id).classList.add(className);
}

function _removeClass(id, className) {
  document.getElementById(id).classList.remove(className);
}

function _isClassExists(id, className) {
  return document.getElementById(id).classList.contains(className);
}
