function inputClick(clickedId) {
  console.log('clicked_id: ' + clickedId);

  _toggleClass(clickedId, 'active');

  if((_isClassExists('xor-p', 'active') || _isClassExists('xor-q', 'active')) && !(_isClassExists('xor-p', 'active') && _isClassExists('xor-q', 'active'))) {
    _addClass('xor-out', 'active');
  } else {
    _removeClass('xor-out', 'active');
  }

  _clearBackgroundsInTable();

  var idTable = '';
  idTable += _isClassExists('xor-p', 'active') ? '1' : '0';
  idTable += _isClassExists('xor-q', 'active') ? '1' : '0';
  _addClass(idTable, 'active');
}

function _clearBackgroundsInTable() {
  var el = document.getElementsByTagName('tr');
      
  for (var i = 0; i < el.length; i++) {
    el[i].classList.remove('active');
  }
}

function _toggleClass(id, className) {
  document.getElementById(id).classList.toggle(className);
}

function _addClass(id, className) {
  document.getElementById(id).classList.add(className);
}

function _removeClass(id, className) {
  document.getElementById(id).classList.remove(className);
}

function _isClassExists(id, className) {
  return document.getElementById(id).classList.contains(className);
}
